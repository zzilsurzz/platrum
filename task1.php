<?php

/**
 * Так как в запросе количество id может быть до нескольких миллионов, а ограничение max_allowed_packet в настройках
 * базы 1048576. Есть несколько вариантов.
 * 
 * 1 вариант. Изменить ограничение до необходимого, но не больше 1073741824. 
 * Самый простой, но не самый эффективный вариант
 * 
 * 2 вариант. Изменить метод "loadUsersByIds". Разбить массив $ids на чанки, и в цикле выполнить запрос.
 */

class User
{
    /** 
     * @param int[] $ids 
     * @return array 
     */
    public function loadUsersByIds(array $ids): array
    {
        $result = [];
        foreach (array_chunk($ids, 1000000) as $idsChunk) {
            $idsFilter = $this->_getInCondition('id', $idsChunk);
            $result = array_merge($result, $this->_loadObjectsByFilter('user', [$idsFilter]));
        }
        return $result;
    }
    private function _getInCondition(string $field, array $values): string
    {
        if (count($values) === 0) {
            return '0';
        }

        $uniqueValues = array_unique(array_filter($values));
        $quotedValues = array_map(fn (string $value) => $this->_quoteString($value), $uniqueValues);
        $implodedValues = implode(', ', $quotedValues);
        $quotedField = "`{$field}`";
        return "({$quotedField} IN ({$implodedValues}) AND {$quotedField} IS NOT NULL";
    }
    private function _quoteString(string $value): string
    {
        $conn = new PDO('');
        return $conn->quote($value);
    }

    private function _loadObjectsByFilter(string $objectName, array $filter = [])
    {
        //some PDO work, result query will be SELECT * FROM $objectName WHERE ($f 30
        return [];
    }
}
